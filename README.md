# semantic-release-ci

semantic-release-ci is a docker image for semantic-release tasks.
The image is based on node:12-buster-slim and contains
- git-core
- ca-certificates 
- openssh-client

default workspace is `/opt/workspace`
default user is `gitlab-runner` with uid `1001`
