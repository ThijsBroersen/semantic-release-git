FROM node:12-buster-slim

RUN apt-get update && apt-get install -y --no-install-recommends git-core ca-certificates openssh-client && \
	npm install -g semantic-release

RUN rm -rf /var/lib/apt/lists/*

# Add a new user "gitlab-runner" with user id 1001
RUN useradd -m -u 1001 gitlab-runner
# Change to non-root privilege
USER gitlab-runner

WORKDIR /opt/workspace
